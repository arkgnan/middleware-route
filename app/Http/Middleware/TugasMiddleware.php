<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class TugasMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        switch(Auth::user()->userRole()){
            case 1:
                $role = "Admin";
                $arr_url = array('route-1', 'route-2', 'route-3');
                break;
            case 2:
                $role = "Super Admin";
                $arr_url = array('route-2', 'route-3');
                break;
            default: 
                $role = 'Guest';
                $arr_url = array('route-3');
        }

        $url = $request->route()->uri;
        // dd($url);
        if(in_array($url, $arr_url)){

            return $next($request);
        }

        abort(403, $role." Hanya bisa mengakses halaman ".implode(", ",$arr_url));
    }
}
