<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TugasController extends Controller
{
    public function superadmin(){
        return "Berhasil masuk halaman superadmin";
    }

    public function admin(){
        return "Berhasil masuk halaman admin";
    }

    public function guest(){
        return "Berhasil masuk halaman guest";
    }
}
