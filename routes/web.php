<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('welcome');
});

// belajar middleware
// Route::get('/test', 'TestController@test')->middleware('dateMiddleware');
// Route::middleware('dateMiddleware')->group(function(){
//     Route::get('/test', 'TestController@test');
// });

// Route::get('/test1', 'TestController@test2');

// Route::middleware(['auth', 'admin'])->group(function(){
//     Route::get('/admin', 'TestController@admin');  
// });
Route::middleware('tugas')->group(function(){
    Route::get('/route-1', 'TugasController@superadmin');
    Route::get('/route-2', 'TugasController@admin');
    Route::get('/route-3', 'TugasController@guest');
});

// tugas 2



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
